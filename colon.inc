%define end_of_dict 0
%macro colon 2
    %2: dq end_of_dict
    db %1, 0
    %define end_of_dict %2
%endmacro