ASM = nasm -felf64 -o
LOAD = ld -o
PYTHON = python3

programm: lib.o main.o dict.o
	$(LOAD) programm main.o lib.o dict.o
	rm *.o

%.o: %.asm
	${ASM} $@ $^

dict.o: dict.asm lib.inc
main.o: main.asm lib.inc words.inc dict.inc

clean:
	rm -f *.o programm 

test: programm
	$(PYTHON) test.py

.PHONY: clean test