
import subprocess
import os

inputs = ["Java", "Go", "Kotlin", "php", "uresolvedword", "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"]
outputs = ["Java_language", "Go", "Kotlin_language", "Php_languauge", "", ""]
errors = ["", "", "", "", "Cant find word", "Invalid data"]


testFaults = []

for i in range(len(inputs)):
    p = subprocess.Popen(["./programm"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate(input=inputs[i].encode())
    out = out.decode().strip()
    err = err.decode().strip()
    if out == outputs[i] and err == errors[i]:
        print("-", end="")
    else:
        print("F", end="")
        testFaults.append([i, inputs[i], out, outputs[i], err, errors[i]])
print("\n")
for i in testFaults:
    print("----------------------------")
    print("Fault when exeuting test " + str(i[0]) + " with \"" + i[1] + "\" parameters.")
    print("Expected: (strout) \"" + i[3] + "\" , recieved \"" + i[2] + "\"")
    print("          (strerr) \"" + i[5] + "\" , recieved \"" + i[4] + "\"")
