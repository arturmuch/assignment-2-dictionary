
global _start

%include "words.inc" 
%include "lib.inc"
%include "dict.inc"

%define BUFFER_SIZE 256

section .bss
BUFFER: resb BUFFER_SIZE 

section .rodata

inputErr: db "Invalid data", 10,  0
findingErr: db "Cant find word", 10, 0

section .text

_start:
    mov rdi, BUFFER
    mov rsi, BUFFER_SIZE
    call read_word
    test rax, rax
    je .read_error
    mov rdi, rax
    mov rsi, php
    call find_word
    test rax, rax
    je .find_error

    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    xor rdi, rdi
    call print_newline
    jmp .out

.read_error:
    mov rdi, inputErr
    call print_fail
    mov rdi, 1
    jmp .out
.find_error:
    mov rdi, findingErr
    call print_fail
    mov rdi, 1
.out:
    jmp exit