%include "lib.inc"
global find_word

find_word:
    push r12
    push r13
     
    test rdi, rdi
    je .return_zero
.cycle:
    test rsi, rsi
    je .return_zero
    mov r12, rdi
    mov r13, rsi
    lea rsi, [rsi + 8]
    call string_equals
    mov rsi, r13
    mov rdi, r12
    test rax, rax
    jne .success
    mov rsi, [rsi]
    jmp .cycle
.return_zero:
    xor rax, rax
    jmp .exit
.success:
    xor rax, rsi
    jmp .exit
.exit:
    pop r13
    pop r12
    ret